package com.space.game.artifacts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.space.game.support.AssetManager;
import com.space.game.utils.Settings;

import java.util.ArrayList;

public class Bullet extends Actor {
    private Rectangle balaCollision;
    private ShapeRenderer shapeRenderer;
    private Vector2 position;
    private int width, height;
    private ScrollHandler scrollHandler;

    public Bullet(Vector2 position, ScrollHandler scrollHandler) {
        this.scrollHandler = scrollHandler;
        this.shapeRenderer = new ShapeRenderer();
        this.position = position;
        balaCollision = new Rectangle();
        setBounds(position.x, position.y, width, height);
    }

    public TextureRegion getBalaTexture(){
        return AssetManager.bullet;
    }
    public Rectangle getBalaCollision() {return balaCollision;}

    /**
     * Function to draw the bullet
     * @param batch
     * @param parentAlpha
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(getBalaTexture(), position.x, position.y, Settings.BULLET_WIDTH, Settings.BULLET_HEIGHT);
    }

    public boolean colisionBalaAsteroide(ArrayList<Asteroid> asteroides){
        for (Asteroid asteroid: asteroides) {
            if(asteroid.collidesBullet(this)){
                Gdx.app.log("Colision", "colision");
                this.remove();
                asteroid.setVisible(false);
                asteroid.setContact(false);

                return true;
            }
        }
        return false;
    }

    /**
     * Function to do some actions with the bullet (shoot)
     * @param delta
     */
    @Override
    public void act(float delta) {
        super.act(delta);
        if(position.x + Settings.BULLET_VELOCITY*delta<=Settings.GAME_WIDTH){
            position.x+= Settings.BULLET_VELOCITY*delta;
            balaCollision.set(position.x, position.y, width, height+2);
            colisionBalaAsteroide(scrollHandler.getAsteroids());
        }else{
            this.remove();
        }
    }


}