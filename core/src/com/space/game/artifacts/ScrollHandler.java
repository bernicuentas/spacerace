package com.space.game.artifacts;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.space.game.screen.Background;
import com.space.game.support.AssetManager;
import com.space.game.utils.Methods;
import com.space.game.utils.Settings;

import java.util.ArrayList;
import java.util.Random;

public class ScrollHandler extends Group {
    Background bg, bg_back;
    int numAsteroids;
    private ArrayList<Asteroid> asteroids;
    Random r;

    /**
     * Intitialize the array of Asteroids and draw them
     */
    public ScrollHandler() {
        bg = new Background(0, 0, Settings.GAME_WIDTH * 2, Settings.GAME_HEIGHT, Settings.BG_SPEED);
        bg_back = new Background(bg.getTailX(), 0, Settings.GAME_WIDTH * 2, Settings.GAME_HEIGHT, Settings.BG_SPEED);
        addActor(bg);
        addActor(bg_back);
        r = new Random();
        numAsteroids = 3;
        asteroids = new ArrayList<Asteroid>();
        float newSize = Methods.randomFloat(Settings.MIN_ASTEROID, Settings.MAX_ASTEROID) * 34;

        Asteroid asteroid = new Asteroid(Settings.GAME_WIDTH, r.nextInt(Settings.GAME_HEIGHT - (int) newSize), newSize, newSize, Settings.ASTEROID_SPEED, true);
        asteroids.add(asteroid);
        addActor(asteroid);

        for (int i = 1; i < numAsteroids; i++) {
            newSize = Methods.randomFloat(Settings.MIN_ASTEROID, Settings.MAX_ASTEROID) * 34;
            asteroid = new Asteroid(asteroids.get(asteroids.size() - 1).getTailX() + Settings.ASTEROID_GAP, r.nextInt(Settings.GAME_HEIGHT - (int) newSize), newSize, newSize, Settings.ASTEROID_SPEED, true);
            asteroids.add(asteroid);
            addActor(asteroid);
        }
    }

    /**
     * Function to check if the object is out
     * @param delta
     */
    @Override
    public void act(float delta) {
        super.act(delta);
        if (bg.isLeftOfScreen()) {
            bg.reset(bg_back.getTailX());
        } else if (bg_back.isLeftOfScreen()) {
            bg_back.reset(bg.getTailX());
        }

        for (int i = 0; i < asteroids.size(); i++) {
            Asteroid asteroid = asteroids.get(i);
            if (asteroid.isLeftOfScreen()) {
                if (i == 0) {
                    asteroid.reset(asteroids.get(asteroids.size() - 1).getTailX() + Settings.ASTEROID_GAP);
                } else {
                    asteroid.reset(asteroids.get(i - 1).getTailX() + Settings.ASTEROID_GAP);
                }
            }
        }
    }

    /**
     * Control the collision between the spacechip and the asteroids
     * @param ship
     * @return
     */
    public boolean collides(SpaceCraft ship) {
        for (Asteroid asteroid : asteroids) {
            if (asteroid.collides(ship)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Settings to the easy level
     */
    public void nEasy(){
        bg.velocity=Settings.BG_SPEED+30;
        bg_back.velocity= Settings.BG_SPEED+30;
        for(Asteroid asteroid:asteroids){
            asteroid.velocity=Settings.ASTEROID_SPEED+30;
        }
    }

    /**
     * Settings to the medium level
     */
    public void nMedium(){
        bg.velocity=Settings.BG_SPEED;
        bg_back.velocity= Settings.BG_SPEED;
        for(Asteroid asteroid:asteroids){
            asteroid.velocity=Settings.ASTEROID_SPEED;
        }
    }

    /**
     * Settings to the hard level
     */
    public void nHard(){
        bg.velocity=Settings.BG_SPEED-10;
        bg_back.velocity= Settings.BG_SPEED-10;

        for(Asteroid asteroid:asteroids){
            asteroid.velocity=Settings.ASTEROID_SPEED-10;
        }
    }

    public void pause(){
        AssetManager.music.pause();
        bg.velocity=0;
        bg_back.velocity=0;
        for(Asteroid asteroid:asteroids){
            asteroid.velocity=0;
        }
    }

    public void resume(String dificultad){
        AssetManager.music.play();
        if(dificultad.equals("easy")) {
            nEasy();
        } else if (dificultad.equals("medium")){
            nMedium();
        } else if (dificultad.equals("hard")){
            nHard();
        }
    }

    public void reset() {
        asteroids.get(0).reset(Settings.GAME_WIDTH);

        for (int i = 1; i < asteroids.size(); i++) {
            asteroids.get(i).reset(asteroids.get(i - 1).getTailX() + Settings.ASTEROID_GAP);
        }
    }

    public ArrayList<Asteroid> getAsteroids() {
        return asteroids;
    }
}