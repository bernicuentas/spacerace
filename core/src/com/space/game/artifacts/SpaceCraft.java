package com.space.game.artifacts;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.space.game.support.AssetManager;
import com.space.game.utils.Settings;

public class SpaceCraft extends Actor {


    /**
     * Spacecraft distances
     */
    public static final int SPACECRAFT_STRAIGHT = 0;
    public static final int SPACECRAFT_UP = 1;
    public static final int SPACECRAFT_DOWN = 2;

    /**
     * Other pacecraft parameters
     */
    private Vector2 position;
    private int width, height;
    private int direction;
    private Stage stage;
    private ShapeRenderer shapeRenderer;

    private Rectangle collisionRect;

    public SpaceCraft(float x, float y, int width, int height,ShapeRenderer shapeRenderer) {
        this.width = width;
        this.height = height;
        this.shapeRenderer = shapeRenderer;
        position = new Vector2(x, y);
        direction = SPACECRAFT_STRAIGHT;
        collisionRect = new Rectangle();
        setBounds(position.x, position.y, width, height);
        setTouchable(Touchable.enabled);
    }

    /**
     * Function to determine the direction of the spaceship
     * @param delta
     */
    public void act(float delta) {
        switch (direction) {
            case SPACECRAFT_UP:
                if (this.position.y - Settings.SPACECRAFT_VELOCITY * delta >= 0) {
                    this.position.y -= Settings.SPACECRAFT_VELOCITY * delta;
                }
                break;
            case SPACECRAFT_DOWN:
                if (this.position.y + height + Settings.SPACECRAFT_VELOCITY * delta <= Settings.GAME_HEIGHT) {
                    this.position.y += Settings.SPACECRAFT_VELOCITY * delta;
                }
                break;
            case SPACECRAFT_STRAIGHT:
                break;
        }
        collisionRect.set(position.x, position.y + 3, width, 10);
        setBounds(position.x, position.y, width, height);

    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void goUp() {
        direction = SPACECRAFT_UP;
    }

    public void goDown() {
        direction = SPACECRAFT_DOWN;
    }

    public void goStraight() {
        direction = SPACECRAFT_STRAIGHT;
    }

    /**
     * Returns the texture of the spaceship depending on the direction of the ship
     * @return
     */
    public TextureRegion getSpaceCraftTexture() {
        switch (direction) {
            case SPACECRAFT_STRAIGHT:
                return AssetManager.spacecraft;
            case SPACECRAFT_UP:
                return AssetManager.spacecraftUp;
            case SPACECRAFT_DOWN:
                return AssetManager.spacecraftDown;
            default:
                return AssetManager.spacecraft;
        }
    }

    public void reset() {
        position.x = Settings.SPACECRAFT_STARTX;
        position.y = Settings.SPACECRAFT_STARTY;
        direction = SPACECRAFT_STRAIGHT;
        collisionRect = new Rectangle();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(getSpaceCraftTexture(), position.x, position.y, width, height);
    }

    public Rectangle getCollisionRect() {
        return collisionRect;
    }

    public Vector2 getPosition() {
        return position;
    }
}