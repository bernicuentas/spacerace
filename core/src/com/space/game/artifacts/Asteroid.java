package com.space.game.artifacts;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;

import java.util.Random;

import com.space.game.support.AssetManager;
import com.space.game.utils.Methods;
import com.space.game.utils.Settings;

public class Asteroid extends Scrollable {
    private Circle collisionCircle;
    Random r;
    boolean contact;
    int assetAsteroid;

    public Asteroid(float x, float y, float width, float height, float velocity, boolean contact) {
        super(x, y, width, height, velocity);

        collisionCircle = new Circle();
        this.contact = contact;
        r = new Random();
        assetAsteroid = r.nextInt(15);

        setOrigin();

        RotateByAction rotateAction = new RotateByAction();
        rotateAction.setAmount(-90f);
        rotateAction.setDuration(0.2f);

        RepeatAction repeat = new RepeatAction();
        repeat.setAction(rotateAction);
        repeat.setCount(RepeatAction.FOREVER);

        this.addAction(repeat);
    }

    public void setOrigin() {
        this.setOrigin(width/2 + 1, height/2);
    }


    @Override
    public void act(float delta) {
        super.act(delta);
        collisionCircle.set(position.x + width / 2.0f, position.y + width / 2.0f, width / 2.0f);
    }

    /**
     * Function to reset all, calculate the size of teh Asteroid with a random
     * and put it in the game windows
     * @param newX
     */
    @Override
    public void reset(float newX) {
        super.reset(newX);
        float newSize = Methods.randomFloat(Settings.MIN_ASTEROID, Settings.MAX_ASTEROID);
        width = height = 34 * newSize;
        position.y = new Random().nextInt(Settings.GAME_HEIGHT - (int) height);

        this.setVisible(true);
        this.setContact(true);
        assetAsteroid = r.nextInt(15);
        setOrigin();

        assetAsteroid = r.nextInt(15);
        setOrigin();
    }

    /**
     * We have to draw the Asteroid
     * @param batch
     * @param parentAlpha
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(AssetManager.asteroid[assetAsteroid], position.x, position.y, this.getOriginX(), this.getOriginY(), width, height, this.getScaleX(), this.getScaleY(), this.getRotation());
    }

    /**
     * Function to provide a collision
     * @param nau
     * @return
     */
    public boolean collides(SpaceCraft nau) {

        if (position.x <= nau.getX() + nau.getWidth()) {
            if(contact) {
                return (Intersector.overlaps(collisionCircle, nau.getCollisionRect()));
            }
        }
        return false;
    }

    /**
     * Provides the collision with the bullet on an Asteroid
     * @param bullet
     * @return
     */
    public boolean collidesBullet(Bullet bullet) {
        if(this.isContact()) {
            return (Intersector.overlaps(collisionCircle, bullet.getBalaCollision()));
        } else {
            return false;
        }
    }

    public boolean isContact() {
        return contact;
    }

    public void setContact(boolean contact) {
        this.contact = contact;
    }
}
