package com.space.game.utils;

import java.util.Random;

public class Methods {

    /**
     * Function to randomize a float
     * @param min
     * @param max
     * @return
     */
    public static float randomFloat(float min, float max) {
        Random r = new Random();
        return r.nextFloat() * (max - min) + min;

    }
}