package com.space.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.space.game.SpaceBernat;
import com.space.game.screen.GameScreen;
import com.space.game.support.AssetManager;
import com.space.game.utils.Settings;

public class Difficult implements Screen, EventListener {
    private Stage stage;
    private SpaceBernat game;


    private TextButton easy, medium, hard;

    /**
     * Set the difficult of the game, depeding on the choice of the player
     * @param game
     */
    public Difficult(SpaceBernat game) {
        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = AssetManager.font;
        this.game = game;
        boolean touch = false;

        OrthographicCamera camera = new OrthographicCamera(Settings.GAME_WIDTH, Settings.GAME_HEIGHT);
        camera.setToOrtho(true);
        StretchViewport viewport = new StretchViewport(Settings.GAME_WIDTH, Settings.GAME_HEIGHT, camera);

        stage = new Stage(viewport);
        stage.addActor(new Image(AssetManager.background));

        Image spacecraft = new Image(AssetManager.spacecraft);
        float y = Settings.GAME_HEIGHT / 3;
        spacecraft.addAction(Actions.repeat(RepeatAction.FOREVER, Actions.sequence(Actions.moveTo(0 - spacecraft.getWidth(), y), Actions.moveTo(Settings.GAME_WIDTH, y, 5))));

        stage.addActor(spacecraft);

        easy = new TextButton("EASY", style);
        medium = new TextButton("MEDIUM", style);
        hard = new TextButton("HARD", style);

        Container containereasy = new Container(easy);
        containereasy.setTransform(true);
        containereasy.center();

        containereasy.setPosition(Settings.GAME_WIDTH/2+10, 30);
        Container containermedium = new Container(medium);
        containermedium.setTransform(true);
        containermedium.center();

        containermedium.setPosition(Settings.GAME_WIDTH/2+10, 60);
        Container containerhard = new Container(hard);
        containerhard.setTransform(true);
        containerhard.center();

        containerhard.setPosition(Settings.GAME_WIDTH/2+10, 90);
        stage.addActor(containereasy);
        stage.addActor(containermedium);
        stage.addActor(containerhard);

        easy.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Difficult.this.game.setScreen(new GameScreen(Difficult.this.stage.getBatch(), Difficult.this.stage.getViewport(), "easy"));
                dispose();
            }
        });

        medium.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Difficult.this.game.setScreen(new GameScreen(Difficult.this.stage.getBatch(), Difficult.this.stage.getViewport(), "medium"));
                dispose();
            }
        });

        hard.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Difficult.this.game.setScreen(new GameScreen(Difficult.this.stage.getBatch(), Difficult.this.stage.getViewport(), "hard"));
                dispose();
            }
        });

        Gdx.input.setInputProcessor(stage);
    }
    @Override
    public void show() {
        //NA
    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {
        //NA
    }

    @Override
    public void pause() {
        //NA
    }

    @Override
    public void resume() {
        //NA
    }

    @Override
    public void hide() {
        //NA
    }

    @Override
    public void dispose() {
        //NA
    }

    @Override
    public boolean handle(Event event) {
        return false;
    }
}