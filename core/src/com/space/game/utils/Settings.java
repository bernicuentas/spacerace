package com.space.game.utils;

public class Settings {
        /**
         * Window
         */
        public static final int GAME_WIDTH = 240;
        public static final int GAME_HEIGHT = 135;

        /**
         * Spacecraft
         */
        public static final float SPACECRAFT_VELOCITY = 50;
        public static final int SPACECRAFT_WIDTH = 36;
        public static final int SPACECRAFT_HEIGHT = 15;
        public static final float SPACECRAFT_STARTX = 20;
        public static final float SPACECRAFT_STARTY = GAME_HEIGHT/2 - SPACECRAFT_HEIGHT/2;

        /**
         * Asteroid
         */
        public static final float MAX_ASTEROID = 1.5f;
        public static final float MIN_ASTEROID = 0.5f;
        public static final int ASTEROID_SPEED = -150;
        public static final int ASTEROID_GAP = 75;
        public static final int BG_SPEED = -100;

        /**
         * Bullet
         */
        public static final float BULLET_VELOCITY=SPACECRAFT_VELOCITY *2;
        public static final int BULLET_WIDTH=24;//8
        public static final int BULLET_HEIGHT=6;//2
}

