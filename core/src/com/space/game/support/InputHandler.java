package com.space.game.support;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.space.game.artifacts.Bullet;
import com.space.game.artifacts.ScrollHandler;
import com.space.game.artifacts.SpaceCraft;
import com.space.game.screen.GameScreen;

public class InputHandler implements InputProcessor {
    int previousY = 0;
    private SpaceCraft spacecraft;
    private GameScreen screen;
    private Vector2 stageCoord;
    ScrollHandler scrollHandler;
    private Batch batch;
    private Stage stage;

    public InputHandler(GameScreen screen, ScrollHandler scrollHandler) {
        this.screen = screen;
        spacecraft = screen.getSpaceCraft();
        this.scrollHandler = scrollHandler;
        stage = screen.getStage();
    }

    /**
     * Key control (up/down arrows to movement, space to shoot, right arrow to straight)
     * @param keycode
     * @return
     */
    @Override
    public boolean keyDown(int keycode) {
            switch (keycode) {
                case Input.Keys.SPACE:
                    if(screen.getNumBullets()>0) {
                        Bullet bullet = new Bullet(new Vector2(screen.getSpaceCraft().getPosition().x + screen.getSpaceCraft().getWidth(),
                                screen.getSpaceCraft().getPosition().y + screen.getSpaceCraft().getHeight() / 2), scrollHandler);
                        screen.getStage().addActor(bullet);


                        screen.setNumBullets(screen.getNumBullets() - 1);
                    }
                    break;
                case Input.Keys.UP:
                    spacecraft.goUp();
                    break;
                case Input.Keys.DOWN:
                    spacecraft.goDown();
                    break;
                case Input.Keys.RIGHT:
                    spacecraft.goStraight();
                    break;
                default:
                    spacecraft.goStraight();
            }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    /**
     * Function to set the status of the game and the actions according with it
     * @param screenX
     * @param screenY
     * @param pointer
     * @param button
     * @return
     */
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        switch (screen.getCurrentState()) {

            case READY:
                screen.setCurrentState(GameScreen.GameState.RUNNING);
                break;
            case RUNNING:
                previousY = screenY;
                stageCoord = stage.screenToStageCoordinates(new Vector2(screenX, screenY));
                Actor actorHit = stage.hit(stageCoord.x, stageCoord.y, true);
                if (actorHit != null){
                    Gdx.app.log("HIT", actorHit.getName());
                    
                    if(actorHit.getName().equals("pause")){
                        
                        if(!screen.paused) {
                            screen.getScrollHandler().pause();
                            screen.paused = true;
                        }else {
                            screen.getScrollHandler().resume(screen.dificultad);
                            screen.paused=false;
                        }
                    }
                }else{
                    if(screen.paused) {
                        screen.getScrollHandler().resume(screen.dificultad);
                        screen.paused = false;
                    }else{
                        screen.getScrollHandler();
                    }
                }
                break;
            case GAMEOVER:
                screen.reset();
                break;
        }
        return true;
    }

    /**
     * Function to go straight
     * @param screenX
     * @param screenY
     * @param pointer
     * @param button
     * @return
     */
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        spacecraft.goStraight();
        return true;
    }

    /**
     * Function to receive the movements of the mouse
     * @param screenX
     * @param screenY
     * @param pointer
     * @return
     */
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (Math.abs(previousY - screenY) > 2)
            if (previousY < screenY) {
                spacecraft.goDown();
            } else {
                spacecraft.goUp();
            }
        previousY = screenY;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amount, float a) {
        return false;
    }
}
