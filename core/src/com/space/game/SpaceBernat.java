package com.space.game;

import com.badlogic.gdx.Game;
import com.space.game.screen.SplashScreen;
import com.space.game.support.AssetManager;

public class SpaceBernat extends Game {
	/**
	 * Function to load the configuration and set thw window
	 */
	@Override
	public void create() {
		AssetManager.load();
		setScreen(new SplashScreen(this));
	}

	/**
	 * Delete the charged resources
	 */
	@Override
	public void dispose() {
		super.dispose();
		AssetManager.dispose();
	}
}
