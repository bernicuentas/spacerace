package com.space.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.space.game.artifacts.Asteroid;
import com.space.game.artifacts.ScrollHandler;
import com.space.game.artifacts.SpaceCraft;
import com.space.game.support.AssetManager;
import com.space.game.support.InputHandler;
import com.space.game.utils.Settings;

import java.util.ArrayList;


public class GameScreen implements Screen {
    //Enum with the different states of the game
    public enum GameState {
        READY, RUNNING, GAMEOVER
    }

    private GameState currentState;
    private Stage stage;
    private SpaceCraft spacecraft;
    private ScrollHandler scrollHandler;
    private TextButton pausa;
    private ShapeRenderer shapeRenderer;
    private Batch batch;
    private float explosionTime = 0;
    private GlyphLayout textLayout;
    private GlyphLayout puntuacioText;
    private int puntuacio;
    private int numBullets;
    private int numBulletsMax;
    private int numBulletsSum;

    public int getNumBulletsSum() {
        return numBulletsSum;
    }

    public void setNumBulletsSum(int numBulletsSum) {
        this.numBulletsSum = numBulletsSum;
    }

    public boolean paused=false;
    public String dificultad="";
    private int suma=1;

    /**
     * Declare the start of the game with the music, the objects and the texture. Also we can set
     * the settings of the game depending on the difficulty
     * @param prevBatch
     * @param prevViewport
     * @param dificultad
     */
    public GameScreen(Batch prevBatch, Viewport prevViewport,String dificultad) {
        this.dificultad=dificultad;
        AssetManager.music.play();
        shapeRenderer = new ShapeRenderer();
        stage = new Stage(prevViewport, prevBatch);
        batch = stage.getBatch();
        spacecraft = new SpaceCraft(Settings.SPACECRAFT_STARTX, Settings.SPACECRAFT_STARTY, Settings.SPACECRAFT_WIDTH, Settings.SPACECRAFT_HEIGHT, shapeRenderer);//
        scrollHandler = new ScrollHandler();

        stage.addActor(scrollHandler);
        stage.addActor(spacecraft);
        spacecraft.setName("spacecraft");

        textLayout = new GlyphLayout();
        textLayout.setText(AssetManager.font, "Are you\nready?");

        puntuacioText = new GlyphLayout();
        puntuacio = 0;
        drawPausa();
        currentState = GameState.READY;

        Gdx.input.setInputProcessor(new InputHandler(this,scrollHandler));

        if(dificultad.equals("easy")) {
            scrollHandler.nEasy();
            suma=1;
            numBullets =30;
            numBulletsMax =30;
            numBulletsSum=1;
        } else if (dificultad.equals("medium")){
            scrollHandler.nMedium();
            suma=2;
            numBullets =20;
            numBulletsMax =20;
            numBulletsSum=1;

        } else if (dificultad.equals("hard")){
            scrollHandler.nHard();
            suma=3;
            numBullets =10;
            numBulletsMax =10;
            numBulletsSum=1;
        }
    }

    /**
     * Draw the texture and the button of "Pause"
     */
    private void drawPausa(){
        TextButton.TextButtonStyle estilo = new TextButton.TextButtonStyle();
        estilo.font = AssetManager.font;
        pausa = new TextButton("Pause", estilo);
        pausa.setName("pause");
        pausa.getLabel().setName("pause");

        pausa.setPosition(Settings.GAME_WIDTH - pausa.getWidth()-10,10);
        pausa.setBounds(150, pausa.getMinHeight(), pausa.getWidth(), pausa.getHeight());

        pausa.setTouchable(Touchable.enabled);

        stage.addActor(pausa);
    }

    @Override
    public void show() {
        //NA
    }

    @Override
    public void render(float delta) {
        stage.draw();

        switch (currentState) {
            case GAMEOVER:
                updateGameOver(delta);
                break;
            case RUNNING:
                updateRunning(delta);
                break;
            case READY:
                updateReady();
                break;
        }
    }

    private void updateReady() {
        batch.begin();
        AssetManager.font.draw(batch, textLayout, (Settings.GAME_WIDTH / 2) - textLayout.width / 2, (Settings.GAME_HEIGHT / 2) - textLayout.height / 2);
        batch.end();
    }

    /**
     * Load or set configuration depending on the state of the game
     * @param delta
     */
    private void updateRunning(float delta) {
        batch.begin();
        AssetManager.fontPuntuacio.draw(batch, "" + puntuacio, 10, 2);
        if(!paused) {
            stage.act(delta);
            puntuacio=puntuacio+suma;
            AssetManager.fontPuntuacio.draw(batch, "" + puntuacio, 10, 2);/////
            AssetManager.fontPuntuacio.draw(batch, "Ammo: " + numBullets, 10, 100);/////

            if (scrollHandler.collides(spacecraft)) {
                String verPuntuacion = Integer.toString(puntuacio);
                puntuacio = 0;
                numBullets=numBulletsMax;

                AssetManager.explosionSound.play();
                stage.getRoot().findActor("spacecraft").remove();
                stage.getRoot().findActor("pause").remove();
                textLayout.setText(AssetManager.font, "Game Over\n" +
                        "Points: " + verPuntuacion);
                currentState = GameState.GAMEOVER;
            }
        }
        batch.end();
    }

    public int getNumBullets() {
        return numBullets;
    }

    public void setNumBullets(int numBullets) {
        this.numBullets = numBullets;
    }

    /**
     * Update the game over state
     * @param delta
     */
    private void updateGameOver(float delta) {
        stage.act(delta);
        batch.begin();
        AssetManager.font.draw(batch, textLayout, (Settings.GAME_WIDTH - textLayout.width) / 2, (Settings.GAME_HEIGHT - textLayout.height) / 2);
        batch.draw((TextureRegion)AssetManager.explosionAnim.getKeyFrame(explosionTime, false), (spacecraft.getX() + spacecraft.getWidth() / 2) - 32, spacecraft.getY() + spacecraft.getHeight() / 2 - 32, 64, 64);
        batch.end();
        explosionTime += delta;
    }

    /**
     * Restart the game
     */
    public void reset() {
        textLayout.setText(AssetManager.font, "Are you\nready?");
        spacecraft.reset();
        scrollHandler.reset();
        drawPausa();
        currentState = GameState.READY;
        stage.addActor(spacecraft);
        explosionTime = 0.0f;
    }

    @Override
    public void resize(int width, int height) {
        //NA
    }

    @Override
    public void pause() { /*NA*/    }

    @Override
    public void resume() { /*NA*/   }

    @Override
    public void hide() { /*NA*/     }

    @Override
    public void dispose() {/*NA*/   }

    public SpaceCraft getSpaceCraft() {
        return spacecraft;
    }

    public Stage getStage() {
        return stage;
    }

    public ScrollHandler getScrollHandler() {
        return scrollHandler;
    }

    public GameState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(GameState currentState) {
        this.currentState = currentState;
    }
}

